const express = require('express');
const { key, getRedis } = require('./config');

const { redis } = getRedis;

const PORT = process.env.PORT || 5000;

const app = express();
app.use(express.json());
app.get('/getdata', async (req, res) => {
  try {
    const result = await redis.hvals(key);
    const arr = result.map((x) => JSON.parse(x));
    res.status(200).json({
      success: true,
      data: arr,
    });
  } catch (error) {
    console.error(error);
    res.status(400).json({
      success: false,
      message: 'There is some error!',
    });
  }
});

app.get('/getdata/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const result = await redis.hget(key, id);
    if (result.length > 0) {
      console.log(result);
      res.status(200).json({
        success: true,
        data: JSON.parse(result),
      });
    } else {
      res.status(404).json({
        success: false,
        message: 'Data not found!',
      });
    }
  } catch (error) {
    console.error(error);
    res.status(400).json({
      success: false,
      message: 'There is some error!',
    });
  }
});

app.post('/postdata', async (req, res) => {
  try {
    const { id = new Date().valueOf(), name, age } = req.body;
    console.log(id);
    const data = JSON.stringify({ id, name, age });
    await redis.hset(key, id, data);
    const range = await redis.hlen(key);
    if (range > 10) {
      const firstField = await redis.hkeys(key);
      await redis.hdel(key, firstField[0]);
    }
    res.status(200).json({
      success: true,
      message: 'Data inserted successfully!',
    });
  } catch (error) {
    console.error(error);
    res.status(400).json({
      success: false,
      message: 'There is some error!',
    });
  }
});

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
